from flask import Flask
from flask import Response
from flask import jsonify
import json
from collections import OrderedDict
from flask_swagger import swagger
from flask_jwt import JWT, jwt_required, current_identity

app = Flask(__name__)

_tos_config_path = 'config/tos.json'

class SetEncoder(json.JSONEncoder):
    def default(self, obj):
       if isinstance(obj, set):
          return list(obj)
       if isinstance(obj, dict):
          return dict(obj)
       return json.JSONEncoder.default(self, obj)

@app.route("/tos")
def list_tos():
    """
    Retrieve the metadata of all the available versions of the TOS (Terms of Service)
    ---
    tags:
      - tos
      - list
    definitions:
      - schema:
          id: tos
          properties:
            version:
             type: string
             description: The version of the TOS document
            created:
             type: date
             description: Creation date of the TOS document 
            last_updated:
             type: date
             description: Last update date of the TOS document  
    responses:
      200:
        description: TOS metadata
        schema:
          type: array
          items:
            $ref: '#/definitions/tos'
    """
    tos_config = json.load(open(_tos_config_path), object_pairs_hook=OrderedDict)
    return Response(json.dumps(tos_config), mimetype='application/json')

@app.route("/tos/<version>")
def get_tos(version):
    """
    Retrieve the metadata of the given version of the TOS (Terms of Service)
    ---
    tags:
      - tos
      - version
    parameters:
      - name: version
        in: path
        description: version number
        required: true
        type: string
    responses:
      200:
        description: TOS metadata for the given version
        schema:
          $ref: '#/definitions/tos'
    """    
    tos_config = json.load(open(_tos_config_path), object_pairs_hook=OrderedDict)
    return Response(json.dumps(tos_config[version]), mimetype='application/json')    

@app.route("/tos/latest")
def latest_tos():
    """
    Retrieve the metadata of the latest version of the TOS (Terms of Service)
    ---
    tags:
      - tos
      - latest
    responses:
      200:
        description: TOS metadata for the latest version
        schema:
          $ref: '#/definitions/tos'
    """    
    tos_config = json.load(open(_tos_config_path), object_pairs_hook=OrderedDict)
    tos = tos_config.values()[0]
    with open(tos['path'], 'r') as tos_file:
    	content=tos_file.read()
    	tos['content'] = content
    return Response(json.dumps(tos), mimetype='application/json')    

@app.route("/spec")
def spec():
    swag = swagger(app)
    swag['info']['title'] = "Geniac Application Data"
    swag['info']['version'] = "1.0.0"
    swag['info']['description'] = "The Geniac Application Data service is intended to provide common information to any component of the application. The main purpose of this service is to act as a central point of stateless data in order for other services to request specific information regarding Geniac. The service MUST be protected from unauthorized access, documented via swagger and adhere to Geniac's git flow continuous integration stack."
    swag['info']['contact'] = "Ricardo Garcia (garcric@geniac.com)"
    return jsonify(swag)

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
